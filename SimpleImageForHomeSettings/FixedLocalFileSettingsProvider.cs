﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace System.Configuration
{
    /// <summary>
    /// ユーザースコープ設定をアセンブリのバージョンに依存しないユーザーフォルダに格納する SettingsProvider クラス。
    /// 設定ファイルの場所 "%AppData%/SimpleImageForHome\settings.config" です。
    /// <para>使用方法: プロジェクトのプロパティ [設定] の項目のプロパティ [Provider] に「System.Configuration.FixedLocalFileSettingsProvider」を設定します。</para>
    /// <para>制限事項: ユーザースコープの設定のみ対応。アプリケーションスコープの設定値は ProviderException 例外をスローします。</para>
    /// </summary>
    /// <see cref="https://www.codeproject.com/kb/vb/customsettingsprovider.aspx"/>
    public class FixedLocalFileSettingsProvider : SettingsProvider
	{
		#region コンストラクタ

		/// <summary>
		/// FixedLocalFileSettingsProviderを初期化します。
		/// </summary>
		public FixedLocalFileSettingsProvider()
		{
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 構成時にプロバイダーを参照するために使用される表示名を取得します。
		/// </summary>
		public override string Name
		{
			get { return nameof(FixedLocalFileSettingsProvider); }
		}

		/// <summary>
		/// 現在実行中のアプリケーションの名前を取得または設定します。
		/// </summary>
		/// <value>完全パスや拡張子を含まない、短縮されたアプリケーション名を含んでいる System.String (SimpleAppSettings など)。</value>
		public override string ApplicationName { get; set; }

		private string _UserSettingsFileName;
        /// <summary>
        /// ユーザースコープ設定ファイルのパスを取得します。(%AppData%/SimpleImageForHome\settings.config)
        /// </summary>
        private string UserSettingsFileName
		{
			get
			{
				if (_UserSettingsFileName == null)
				{
					_UserSettingsFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"SimpleImageForHome\settings.config");

					Debug.WriteLine($"{nameof(FixedLocalFileSettingsProvider)} の設定ファイルパス:\n{_UserSettingsFileName}");
				}
				return _UserSettingsFileName;
			}
		}

		private XElement _RootElement;
		/// <summary>
		/// 設定ファイル SettingsFileName のルート要素 settings を取得します。
		/// </summary>
		private XElement RootElement
		{
			get
			{
				if (_RootElement == null)
				{
					_RootElement = GetRootElementOfFile(UserSettingsFileName);
				}
				return _RootElement;
			}
		}

		#endregion

		#region メソッド

		/// <summary>
		/// プロバイダーを初期化します。
		/// </summary>
		/// <param name="name">プロバイダーの表示名</param>
		/// <param name="config">このプロバイダーの構成で指定されている、プロバイダー固有の属性を表す名前と値のペアのコレクション</param>
		public override void Initialize(string name, NameValueCollection config)
		{
			base.Initialize(Name, config);
		}

		/// <summary>
		/// 指定したアプリケーションのインスタンスと設定プロパティ グループの、設定プロパティ値のコレクションを返します。
		/// </summary>
		/// <param name="context">現在のアプリケーションの使い方を記述している System.Configuration.SettingsContext。</param>
		/// <param name="collection">値の取得対象となる設定プロパティ グループを格納している System.Configuration.SettingsPropertyCollection。</param>
		/// <returns>指定した設定プロパティ グループの値を格納している System.Configuration.SettingsPropertyValueCollection。</returns>
		public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
		{
			var root = RootElement;

			var values = new SettingsPropertyValueCollection();
			foreach (SettingsProperty prop in collection)
			{
				if (GetScope(prop) != SettingsScope.User)
					throw new ProviderException($"{nameof(FixedLocalFileSettingsProvider)} はユーザースコープの設定のみ対応しています。");

				var ele = root.Element(prop.Name);
				var value = (ele != null) ? ele.Value : prop.DefaultValue?.ToString() ?? String.Empty;

				values.Add(new SettingsPropertyValue(prop)
				{
					SerializedValue = value
				});
			}
			return values;
		}

		/// <summary>
		/// 指定したプロパティ設定グループの値を設定します。
		/// </summary>
		/// <param name="context">現在のアプリケーションの使い方を記述している System.Configuration.SettingsContext。</param>
		/// <param name="collection">設定するプロパティ設定のグループを表す System.Configuration.SettingsPropertyValueCollection。</param>
		public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
		{
			var root = RootElement;

			foreach (SettingsPropertyValue propValue in collection)
			{
				if (GetScope(propValue.Property) != SettingsScope.User)
					throw new ProviderException($"{nameof(FixedLocalFileSettingsProvider)} はユーザースコープの設定のみ対応しています。");

				var ele = root.Element(propValue.Name);
				if (ele != null)
				{
					ele.Value = propValue.SerializedValue?.ToString() ?? String.Empty;
				}
				else
				{
					ele = new XElement(propValue.Name, propValue.SerializedValue?.ToString() ?? String.Empty);
					root.Add(ele);
				}
			}

			root.Save(UserSettingsFileName);
		
		}

		/// <summary>
		/// 設定項目のスコープを取得します。
		/// </summary>
		/// <param name="property"></param>
		/// <returns></returns>
		private SettingsScope GetScope(SettingsProperty property)
		{
			foreach (var attr in property.Attributes.Values)
			{
				if (attr is UserScopedSettingAttribute)
					return SettingsScope.User;
				if (attr is ApplicationScopedSettingAttribute)
					return SettingsScope.Application;
			}
			return SettingsScope.None;
		}

		/// <summary>
		/// 指定したパスの設定ファイルを開き、ルート要素 settings を取得します。
		/// <para>ファイルまたはルート要素がない場合は作成します。</para>
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		private XElement GetRootElementOfFile(string fileName)
		{
			var dir = Path.GetDirectoryName(fileName);
			if (!Directory.Exists(dir))
			{
				Directory.CreateDirectory(dir);
			}

			XDocument doc = null;
			XElement root = null;

			try
			{
				doc = XDocument.Load(fileName);
				root = doc.Element("settings");
			}
			catch (Exception)
			{
				root = new XElement("settings");
				root.Save(fileName);
			}
			return root;
		}

		#endregion

		#region SettingsScope

		/// <summary>
		/// 設定値のスコープ列挙体
		/// </summary>
		internal enum SettingsScope
		{
			None,
			/// <summary>
			/// ユーザー
			/// </summary>
			User,
			/// <summary>
			/// アプリケーション
			/// </summary>
			Application,
		}

		#endregion
	}
}
