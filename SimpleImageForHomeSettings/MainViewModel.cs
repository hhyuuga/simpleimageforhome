﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SimpleImageForHomeSettings
{
	public class MainViewModel : ViewModelBase
	{
		#region コンストラクタ

		/// <summary>
		/// MainViewModelを初期化します。
		/// </summary>
		public MainViewModel()
		{
			this.CancelCommand = new RelayCommand(ExecuteCancel);
			this.OkCommand = new RelayCommand(ExecuteOk, CanExecuteOk);
			this.RefCommand = new RelayCommand(ExecuteRef);

			var settings = Properties.Settings.Default;
			this.SaveDir = settings.SaveDir;
			this.Digit = settings.Digit;
		}

		#endregion

		#region プロパティ

		private string m_SaveDir;
		/// <summary>
		/// 保存先パスを取得または設定します。
		/// </summary>
		public string SaveDir
		{
			get { return m_SaveDir; }
			set
			{
				if (m_SaveDir == value)
					return;
				m_SaveDir = value;
				RaisePropertyChanged();
				this.OkCommand.RaiseCanExecuteChanged();
			}
		}

		private int m_Digit = 3;
		/// <summary>
		/// 連番桁数を取得または設定します。
		/// </summary>
		public int Digit
		{
			get { return m_Digit; }
			set
			{
				if (m_Digit == value)
					return;
				m_Digit = value;
				RaisePropertyChanged();
			}
		}

		#endregion

		#region コマンド

		public RelayCommand OkCommand { get; private set; }
		/// <summary>
		/// OKコマンド
		/// </summary>
		private void ExecuteOk()
		{
			// TODO

			var settings = Properties.Settings.Default;
			settings.SaveDir = this.SaveDir.Trim();
			settings.Digit = this.Digit;
			settings.Save();

			App.Current.MainWindow.Close();
		}
		private bool CanExecuteOk()
		{
			return !String.IsNullOrWhiteSpace(this.SaveDir);
		}

		public RelayCommand CancelCommand { get; private set; }
		/// <summary>
		/// キャンセルコマンド
		/// </summary>
		private void ExecuteCancel()
		{
			App.Current.MainWindow.Close();
		}

		public RelayCommand RefCommand { get; private set; }
		/// <summary>
		/// 保存先フォルダの参照ボタンコマンド
		/// </summary>
		private void ExecuteRef()
		{
			using (var dialog = new CommonOpenFileDialog() { IsFolderPicker = true, InitialDirectory = this.SaveDir })
			{
				var result = dialog.ShowDialog(App.Current.MainWindow);
				if (result == CommonFileDialogResult.Ok)
				{
					this.SaveDir = dialog.FileName;
				}
			}
		}

		#endregion
	}
}
