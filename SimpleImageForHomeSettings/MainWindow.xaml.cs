﻿using GalaSoft.MvvmLight.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleImageForHomeSettings
{
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			this.DataContext = new MainViewModel();

			DispatcherHelper.Initialize();

			this.Loaded += OnLoaded;
		}

		private async void OnLoaded(object sender, RoutedEventArgs e)
		{
			this.Loaded -= OnLoaded;

			await Task.Delay(1);

			await DispatcherHelper.RunAsync(() => {
				TextBoxSaveDir.SelectAll();
			});
		}

		private void TextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			TextBoxSaveDir.SelectAll();
		}
	}
}
