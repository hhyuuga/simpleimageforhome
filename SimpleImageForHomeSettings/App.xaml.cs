﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace SimpleImageForHomeSettings
{
	/// <summary>
	/// App.xaml の相互作用ロジック
	/// </summary>
	public partial class App : Application
	{
		public App()
		{
			AppDomain.CurrentDomain.AssemblyResolve += (sender, e) =>
			{
				var thisAssembly = Assembly.GetExecutingAssembly();
				var dllName = new AssemblyName(e.Name).Name + ".dll";

				var resources = thisAssembly.GetManifestResourceNames().Where(s => s.EndsWith(dllName));
				if (resources.Any())
				{
					var resourceName = resources.First();
					using (var stream = thisAssembly.GetManifestResourceStream(resourceName))
					{
						if (stream == null)
							return null;
						try
						{
							var buffer = new byte[stream.Length];
							stream.Read(buffer, 0, buffer.Length);
							return Assembly.Load(buffer);
						}
						catch (System.IO.IOException)
						{
							return null;
						}
						catch (BadImageFormatException)
						{
							return null;
						}
					}
				}
				return null;
			};
		}
	}
}
