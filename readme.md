## はじめに

SimpleImage for Home は ScanSnap Home と連携して連番ファイル名でファイル保存するシンプルな連携アプリケーションです。
スキャンしたファイルを 001.jpg から始まる連番ファイル名で保存します。

- スキャンファイルを ScanSnap Home へ登録しません。
- 画像以外の情報を無視します。
- 連番ファイルに空き番号があれば埋めます。

## 導入手順

1. SimpleImageForHome.reg の「workdir」をバイナリの配置フォルダパスに編集し、レジストリに登録します。

2. ScanSnap Home のプロファイル編集画面で「SimpleImage for Home」テンプレートから新しいプロファイルを作成します。
![](images/01.png)
![](images/02.png)
3. プロファイルの連携アプリケーションの設定を開き、保存先を設定します。
![](images/03.png)




