﻿using Ini.Net;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleImageForHome
{
	static class Program
	{
		static Program()
		{
			#region AssemblyResolve

			AppDomain.CurrentDomain.AssemblyResolve += (sender, e) =>
			{
				var thisAssembly = Assembly.GetExecutingAssembly();
				var dllName = new AssemblyName(e.Name).Name + ".dll";

				var resources = thisAssembly.GetManifestResourceNames().Where(s => s.EndsWith(dllName));
				if (resources.Any())
				{
					var resourceName = resources.First();
					using (var stream = thisAssembly.GetManifestResourceStream(resourceName))
					{
						if (stream == null)
							return null;
						try
						{
							var buffer = new byte[stream.Length];
							stream.Read(buffer, 0, buffer.Length);
							return Assembly.Load(buffer);
						}
						catch (System.IO.IOException)
						{
							return null;
						}
						catch (BadImageFormatException)
						{
							return null;
						}
					}
				}
				return null;
			};

			#endregion

			#region log4net 初期化

			new Action(() =>
			{
				// ログの出力先を log4net に指定
				log4net.GlobalContext.Properties["LogFileName"] = Path.Combine(Path.GetTempPath(), @"SimpleImageForHome.log");
				// log4net に app.config の構成を読み込む。(assembly 属性)
				log4net.Config.XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile));
			})();

			#endregion
		}

		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			mutex = new Mutex(false, nameof(SimpleImageForHome));
			if (mutex.WaitOne(0, false))
			{
				ProcessScanResult(args.ElementAtOrDefault(0));

				mutex.Close();
			}
		}

		#region プロパティ

		private static Mutex mutex;

		#endregion

		#region メソッド

		/// <summary>
		/// メッセージをコンソールとデバッグ出力に出力します。
		/// </summary>
		/// <param name="message"></param>
		/// <param name="error"></param>
		private static void WriteLine(string message, bool error = false)
		{
			var color = Console.ForegroundColor;
			if (error)
			{
				Console.ForegroundColor = ConsoleColor.Cyan;
			}

			Debug.WriteLine(message);
			Console.WriteLine(message);

			Console.ForegroundColor = color;
		}

		/// <summary>
		/// ScanSnap Home のスキャン結果ファイルを読み込み、スキャンファイルを保存先に移動します。
		/// </summary>
		/// <param name="resultFilePath"></param>
		private static void ProcessScanResult(string resultFilePath)
		{
			var logger = LogManager.GetLogger(String.Empty);

			try
			{
				logger.Info("Start");

				if (!File.Exists(resultFilePath))
					throw new ArgumentException($"スキャン結果ファイル '{resultFilePath}' が見つかりません。");

				var settings = Properties.Settings.Default;
				var saveDir = settings.SaveDir?.Trim();
				var digit = settings.Digit;

				if (String.IsNullOrWhiteSpace(saveDir))
				{
					throw new InvalidOperationException("保存先が設定されていません。");
				}
				if (!Directory.Exists(saveDir))
				{
					try
					{
						Directory.CreateDirectory(saveDir);
					}
					catch (Exception ex)
					{
						throw new IOException($"保存先フォルダ '{saveDir}' の作成時にエラーが発生しました。\n{ex.Message}");
					}
				}

				// スキャン結果ファイルからスキャン画像ファイルのリストを読み込み、保存先 saveDir に移動
				var ini = new IniFile(resultFilePath);
				var count = ini.ReadInteger("FILES", "FileCount");
				logger.Info($"FileCount = {count}");

				//Debugger.Launch();

				for (var i = 0; i < count; i++)
				{
					// スキャンファイルのパス
					var path = ini.ReadString("FILES", $"File{i + 1}");

					// 保存先の空き番ファイルとして移動
					MoveFile(path, saveDir, digit, logger);
				}
			}
			catch (Exception ex)
			{
				var message = $"[{ex.GetType()}] {ex.Message}";
				WriteLine(message, true);
				logger.Error(message);
			}
			finally
			{
				LogManager.Shutdown();

				// 一時フォルダを削除
				var tempDir = Path.GetDirectoryName(resultFilePath);
				if (Directory.Exists(tempDir))
				{
					Directory.Delete(tempDir, true);
				}
			}
		}


		/// <summary>
		/// 指定されたファイルを移動先フォルダ toDir の空いている連番名に移動します。
		/// </summary>
		/// <param name="sourcePath">移動するファイル</param>
		/// <param name="toDir">移動先フォルダのパス</param>
		/// <param name="digit">連番の桁数</param>
		private static void MoveFile(string sourcePath, string toDir, int digit, ILog logger)
		{
			if (String.IsNullOrEmpty(sourcePath))
				throw new ArgumentNullException(nameof(sourcePath));
			if (String.IsNullOrEmpty(toDir))
				throw new ArgumentNullException(nameof(toDir));
			if (digit <= 0)
				throw new ArgumentOutOfRangeException($"{nameof(digit)} は 1 以上の値である必要があります。");

			// 1 から始まる空き番号のファイル名を求める。
			var ext = Path.GetExtension(sourcePath);
			var format = new String('0', digit);

			var newPath = String.Empty;
			for (var i = 1; i < 99999999; i++)
			{
				newPath = Path.Combine(toDir, i.ToString(format) + ext);
				if (!File.Exists(newPath))
					break;
			}

			if (File.Exists(newPath))
				throw new Exception($"保存先 '{toDir}' に空き番が見つかりませんでした。");

			logger.Info($"ファイル移動 {newPath}");
			File.Move(sourcePath, newPath);
		}

		#endregion
	}
}
